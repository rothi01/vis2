import os
import sys
def include_libs(path):
   """
   include alle *.jar bzw. Ordner im path in den Classpath
   """
   for dirname, subdirs, filenames in os.walk(path):
      for filename in filenames:
	      if filename.endswith(".jar"):
		      sys.path.append(os.path.join(dirname,filename))
      for subdir in subdirs:
	      sys.path.append(os.path.join(dirname,subdir))

include_libs('.')

from com.itextpdf.text.pdf.parser import TextRenderInfo
from com.itextpdf.text.pdf.parser import ImageRenderInfo

from src import utilities
from src.readability import BaseFeature
from src import PDFReader
			
reader = PDFReader.PDFReader()


