from src.readability import BaseFeature
from src import TextAnalyzer

class AvgWordLen(BaseFeature.BaseFeature):
   def __init__(self):
      self.name = 'WortLaenge'
   def analysiereSatz(self, satz):
      """
      Analysiere einen Satz (String) nach druchschnittlicher wortlaenge
      Laut googelsuche ist das durchschnittliche englische wort 5.1 letter lang
      siehe http://answers.yahoo.com/question/index?qid=20080526032554AAB28AF

      annahme leicht 0: <= 4 letter
      annahme schwer 1: >= 8 letter
      @param satz - Ein Satz als Zeichenkette
      @return Wert zwischen 0 (leicht) und 1 (schwer)
      """

      avg = 5.1
      max_len = 8
      min_len = 4
      worte = satz.split(' ')
      lengths = []
      for wort in worte:
         lengths.append(len(wort))
      avglen = self.mean(lengths)
      if(avglen == avg):
         return 0.5
      elif(avglen <= min_len):
         return 0
      elif (avglen >= max_len):
         return 1
      elif avglen < avg :
         return self.linearRatio(min_len,avg,avglen)/2
      else:
         return (self.linearRatio(avg,max_len,avglen)/2)+0.5
      
   def linearRatio(self, startval, stopval, actval):
      return (actval-startval) / (stopval - startval)   


