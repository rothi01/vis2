from src.readability import BaseFeature
from src import TextAnalyzer

class CommonWordRatio(BaseFeature.BaseFeature):
   def __init__(self):
      self.name = 'CommonWords'
      f = open("res/common.txt","r")
      lines = f.readlines()
      f.close()
      words = []
      for l in lines:
         words.append(l.lower()[0:-1])
      self.words = words
   def analysiereSatz(self, satz):
      """
      Analysiere einen Satz (String) nach dem anteil der common words.

      listen der most common words common.txt
   
      http://www.rupert.id.au/resources/1000-words.php
      Thanks to rupert.russell@acu.edu.au

      @param satz - Ein Satz als Zeichenkette
      @return Wert zwischen 0 (leicht) und 1 (schwer)
      """
     
      worte = satz.split(' ')
      satzlen = len(worte)
      counter = 0
      for wort in worte:
         if(wort.lower().strip('-.!?,;:"') in self.words):
            counter+=1
      return float(1)-(float(counter)/float(satzlen))
      
   def linearRatio(self, startval, stopval, actval):
      return (actval-startval) / (stopval - startval)   


