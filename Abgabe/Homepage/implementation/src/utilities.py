import java
from javax.swing import JOptionPane
from java.io import File
from javax.swing import JFileChooser


def msgBox(message='',title='Attention',type='plain'):
	"""
	msgBox()
	msgBox("Hello World")
	msgBox("Hello World", "Title")
	msgBox("Hello World", "Title", "plain")
	msgBox(message="Hello World", title="Title", type="plain")
	
	types = {}
	types['info'] = JOptionPane.INFORMATION_MESSAGE
	types['warn'] = JOptionPane.WARNING_MESSAGE
	types['quest'] = JOptionPane.QUESTION_MESSAGE
	types['plain'] = JOptionPane.PLAIN_MESSAGE
	types['error'] = JOptionPane.ERROR_MESSAGE
	"""
	types = {}
	types['info'] = JOptionPane.INFORMATION_MESSAGE
	types['warn'] = JOptionPane.WARNING_MESSAGE
	types['quest'] = JOptionPane.QUESTION_MESSAGE
	types['plain'] = JOptionPane.PLAIN_MESSAGE
	types['error'] = JOptionPane.ERROR_MESSAGE
	JOptionPane.showMessageDialog(None,message,title,types[type])

def FileChooser(path='.', filt=''):
   """
   Note: source for ExampleFileFilter can be found in FileChooserDemo,
   under the demo/jfc directory in the Java 2 SDK, Standard Edition.
   """
   chooser = JFileChooser()
   file_ = File(path)
   chooser.setCurrentDirectory(file_)
   returnVal = chooser.showOpenDialog(None)
   if(returnVal == JFileChooser.APPROVE_OPTION):
      return chooser.getSelectedFile()
   
