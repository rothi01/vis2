
from com.itextpdf.text.pdf import PdfReader
from com.itextpdf.text.pdf.parser import PdfTextExtractor
from com.itextpdf.text.pdf.parser import PdfReaderContentParser
from com.itextpdf.text.pdf.parser import SimpleTextExtractionStrategy

from src import MyRenderListener
from src import Window
from src.readability import BaseFeature
from src.readability import AvgWordLen
from src.readability import AvgSentenceLen
from src.readability import CommonWordRatio

class PDFReader:
   """
   PDFReader
   zustaendig fuer die organisation des Programs
   steuert die gui
   Neue Features in __init__ hinzufuegen!
   """
   def __init__(self):
      """
      Kostruktor
      Feature Instanzierung
      GUI Instanzierung
      """
      self.features = []
      self.ratings = []
      self.features.append(AvgWordLen.AvgWordLen())
      self.features.append(AvgSentenceLen.AvgSentenceLen())
      self.features.append(CommonWordRatio.CommonWordRatio())
      #self.features.append(BaseFeature.BaseFeature())
      self.fenster = Window.Window(self)


   def getBlockRatings(self,pagenr, pos_x, pos_y):
      """
      liefert fuer pagenr (pdfseite beginnend bei 0)
      die Ratings des Blocks der an position (pos_x/pos_y) liegt
      """
      ret = []
      analyzer = self.fenster.pages[pagenr].analyzer
      blockid = analyzer.getBlockId(pos_x,pos_y)
      satzid = analyzer.getSatzId(pos_x,pos_y)
      ids = analyzer.getSatzIdsByBlockId(blockid)
      for feat_ratings in self.ratings:
         rating = BaseFeature.BaseFeature().mean(feat_ratings[pagenr][blockid])
         ret.append(rating)
      return ret

   def getBlockRating(self,pagenr, pos_x, pos_y):
      """
      liefert fuer pagenr (pdfseite beginnend bei 0)
      das gemittelte Rating des Blocks der an position (pos_x/pos_y) liegt
      """
      r = self.getBlockRatings(pagenr,pos_x,pos_y)   
      return BaseFeature.BaseFeature().mean(r)

   def getSatzRatings(self,pagenr, pos_x, pos_y):
      """
      liefert fuer pagenr (pdfseite beginnend bei 0)
      die Ratings des Satzes der an position (pos_x/pos_y) liegt
      """
      analyzer = self.fenster.pages[pagenr].analyzer
      satzid = analyzer.getSatzId(pos_x,pos_y)
      blockid = analyzer.getBlockId(pos_x,pos_y)
      return self.getSatzRatingsById(pagenr,satzid)

   def getSatzRatingsById(self,pagenr, satzid):
      """
      liefert fuer pagenr (pdfseite beginnend bei 0)
      die Ratings des Satzes mit dem index (satzid)
      """
      ret = []
      analyzer = self.fenster.pages[pagenr].analyzer
      try:
         blockid = analyzer.satz_blockMapping[satzid]
      except:
         print pagenr, satzid
         print analyzer.satz_blockMapping
         print analyzer.block_satzMapping
      satzid = satzid
      ids = analyzer.getSatzIdsByBlockId(blockid)
      blockintern_index = -1
      #pruefe ob satzid in den satzids des blocks enthalten ist
      for i in range(len(ids)):
         if(ids[i] == satzid):
            blockintern_index = i
            break

      #gespeichertes rating fuer seite,block,satz abfragen
      for feat_ratings in self.ratings:
         try:
            rating = feat_ratings[pagenr][blockid][blockintern_index]
         except:
            print analyzer.getSatzIdsByBlockId(blockid)
            print ids
            print satzid
         ret.append(rating)
      return ret



   def getSatzRating(self,pagenr, pos_x, pos_y):
      """
      liefert fuer pagenr (pdfseite beginnend bei 0)
      das gemittelte Rating des Satzes der an position (pos_x/pos_y) liegt
      """
      r = self.getSatzRatings(pagenr,pos_x,pos_y)   
      #print r
      return BaseFeature.BaseFeature().mean(r)

   def parseFile(self, pfad):
      """
      callback funktion fuer gui
      Wird aufgerufen wenn ein file mit dem file-open dialog geoeffnet wird

      oeffnet das file
      instanziert einen parser
      analysiert alle seiten (kann bei langen pdfs und vielen features etwas dauern)
      
      setzt setzt zu letzt die erste Seite in der GUI als ausgewaehlt
      """
      reader = PdfReader(pfad)
      parser = PdfReaderContentParser(reader)
      print "Dieses Dokument hat", reader.getNumberOfPages(), "Seiten"
      #seitenuebersicht reseten
      self.fenster.resetPageOverview(reader.getNumberOfPages())
      for f in range(len(self.features)):
         self.ratings.insert(f,[])
      for page in self.fenster.pages:
         page.setParser(parser)   
         for f in range(len(self.features)):
            #(page.pagenr -1) weil die pagenr bei 1 zu zaehlen beginnt
            self.ratings[f].insert(page.pagenr-1,self.features[f].analysiereSeite(page.analyzer))
      self.fenster.setSelectedPage(parser,0)


