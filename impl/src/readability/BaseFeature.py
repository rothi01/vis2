import random
from src import utilities

class BaseFeature:
   """
   Basis-Klasse fuer Features
   
   Stellt Implementierungen der Funktionalitaeten zur Verfuegung
   """
   def __init__(self):
      self.name = 'BaseFeature'

   def analysiereSatz(self, satz=''):
      """
      Analysiere einen Satz (String)
      @param satz - Ein Satz als Zeichenkette
      @return Wert zwischen 0 (leicht) und 1 (schwer)
      """
      return random.random()

   def analysiereBlock(self, block=['']):
      """
      analysiere block
      durchlaueft die saetze des blocks und ruft analysiereSatz auf
      """
      rating = list()
      for satz in block:
         rating.append(self.analysiereSatz(satz))
      return rating
   
   def analysiereSeite(self, analyzer):
      """
      analysiereSeite
      durchlaeuft die seite und ruft fuer jeden block analysiereBlock auf
      """
      rating = list()
      for blockid in range(int(analyzer.getBlockCount())):
         block = analyzer.getBlockById(blockid)
         rating.append(self.analysiereBlock(block))
      return rating

   def analysiereDokument(self, doc=''):
      """
      analysiereDokument
      durchlaeuft das Dokument und ruft fuer jede Seite analysiereSeite auf

      sollte mal ein dokument so gegeben sein kann das ja ganz nuetzlich sein
      """
      rating = list()
      for seite in doc:
         rating.append(self.analysiereSeite(seite))
      return rating


   def mean(self,rating):
      """
      warum gibt es diese funktion nicht schon?!
      """
      #print rating
      return float(sum(rating)) / len(rating)
