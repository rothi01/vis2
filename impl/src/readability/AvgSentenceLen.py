from src.readability import BaseFeature
from src import TextAnalyzer

class AvgSentenceLen(BaseFeature.BaseFeature):
   def __init__(self):
      self.name = 'SatzLaenge'
   def analysiereSatz(self, satz):
      """
      Analysiere einen Satz (String) nach druchschnittlicher satzlaenge
      Laut googelsuche:

      Jyoti Sanyal's Indlish has an interesting and informative chapter titled 'Shrink or sink' on the length of the sentence. 
      He writes: "Based on several studies, press associations in the USA have laid down a readability table. Their survey shows 
      readers find sentences of 
      
      8 words or less very easy to read; 
      11 words, easy; 
      14 words fairly easy; 
      17 words standard; 
      21 words fairly difficult; 
      25 words difficult and 
      29 words or more, very difficult." 

      siehe http://strainindex.wordpress.com/2008/07/28/the-average-sentence-length/

      annahme leicht 0: <= 4 letter
      annahme schwer 1: >= 8 letter
      @param satz - Ein Satz als Zeichenkette
      @return Wert zwischen 0 (leicht) und 1 (schwer)
      """

      difficulty = [8,11,14,17,21,25,29]
      maxlen = max(difficulty)
      minlen = min(difficulty)
      worte = satz.split(' ')
      avglen = len(worte)
      stepsize = float(1)/float(len(difficulty))
      if(avglen <= minlen):
         return 0
      elif (avglen >= maxlen):
         return 1
      else:
         value = float(0)
         for d in difficulty:
            if avglen > d:
               value += stepsize
            else:
               break
      #print value
      return value
      
   def linearRatio(self, startval, stopval, actval):
      return (actval-startval) / (stopval - startval)   


