from src.readability import BaseFeature
from src import TextAnalyzer

class RandomTestFeature(BaseFeature.BaseFeature):
   def analysiereSatz(self, satz):
      """
      Analysiere einen Satz (String)
      @param satz - Ein Satz als Zeichenkette
      @return Wert zwischen 0 (leicht) und 1 (schwer)
      """
      return random.random()

   def analysiereBlock(self, block):
      rating = list()
      for satz in block:
         rating.append(self.analysiereSatz(satz))
      return rating
   

