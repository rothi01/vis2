from javax.swing import JPanel
from java.awt import Dimension
from java.awt import Font
from com.itextpdf.text.pdf.parser import RenderListener
from com.itextpdf.text.pdf.parser import SimpleTextExtractionStrategy
from java.awt.geom import AffineTransform
from java.awt import Rectangle
from java.awt import Color
from java.awt import Graphics2D
import math

class MyCanvas(JPanel, RenderListener):
   """
   MyCanvas
   Renderklasse fuer PDF-Seiten
   fragt ratings ab und macht highlighting
   
   bietet ausserdem eine moeglichkeit farben zu interpolieren
   """
   def __init__(self,pagenr, width,reader):
      JPanel.__init__(self)
      self.reader = reader
      self.pagenr = pagenr
      self.parser = None
      #self.texts = []
      #self.act_text = ''
      self.setWidth(width) 
      #self.s_text = SimpleTextExtractionStrategy()
      
      #rendermode 1: Blockweise highlighting
      #rendermode 0: satzweise highlighting
      self.renderMode = 0

   def paintComponent(self,g):
      """
      ueberlagerung der paintComponent von JPanel
      hier wird das rendern der Seite getriggert
      """
      #Weisser hintergrund
      rec = Rectangle(self.mySize[0], self.mySize[1])
      tmp = g.getColor()
      g.setColor(Color.WHITE)
      g.draw(rec)
      g.fill(rec)
      g.setColor(tmp)
      
      #Nach dem rendern klein scalieren und translieren
      g.scale(self.scale,self.scale)
      g.translate(float(0),self.translate)
      self.g = g

      #rendern mit dem parser
      if self.parser != None:   
         self.parser.processContent(self.pagenr,self)
      

   def setWidth(self,a):
      """
      setze das seitenverhaeltnis
      Verhaeltnis von A4 (generell Ax) = a : (squrt(2)*a)
      """
      #referenz rendergroesse fuer A4 seiten
      #ref_width ... ausgetesteter wert
      ref_Width = 650*0.95*0.96;
      ref_Height = int(math.sqrt(2)*ref_Width)
      h = int(math.sqrt(2)*a)
      self.mySize = [a, h]
      #translier und skalierfaktor berechnen
      self.scale = float(a) / float(ref_Width)
      self.translate = ref_Height - h;
      #panelgroesse setzen
      self.setPreferredSize(Dimension(a,h))

   def renderText(self, renderInfo):
      """
      callback funktion fuer den parser
      hier wird text gerendert
      """
      f = renderInfo.getFont()
      f_ = f.getFullFontName()
      as_start = renderInfo.getAscentLine().getStartPoint()
      ba_start = renderInfo.getBaseline().getStartPoint()
      des_end = renderInfo.getDescentLine().getEndPoint()
      textsize = as_start.get(1) - ba_start.get(1)
      d=self.getSize(Dimension(0,0))
      textcolor = self.g.getColor()
      #Start Highlighting here
      if(self.renderMode == 0):
         rating = self.reader.getSatzRating(self.pagenr-1,ba_start.get(0),ba_start.get(1))
      else:
         rating = self.reader.getBlockRating(self.pagenr-1,ba_start.get(0),ba_start.get(1))
      rating_col = self.linearInterpol3Color(Color.BLUE,Color.WHITE,Color.RED,rating)
      self.g.setColor(rating_col)
      rec = Rectangle(int(as_start.get(0)),int(d.getHeight()-as_start.get(1)),int(des_end.get(0)-as_start.get(0)), int(as_start.get(1)-des_end.get(1)))
      self.g.draw(rec)
      self.g.fill(rec)
      #End Highlighting
      #render text
      self.g.setColor(textcolor)
      self.g.setFont(Font(f_[0][3], Font.PLAIN,int(textsize)))
      self.g.drawString(renderInfo.getText(), ba_start.get(0),d.getHeight()-ba_start.get(1))


   def renderImage(self, renderInfo):
      """
      callback funktion fuer den parser
      hier werden bilder gerendert
      """
      #self.s_text.renderImage(renderInfo)
      d=self.getSize(Dimension(0,0))
      bi = renderInfo.getImage().getBufferedImage() 
      start = renderInfo.getStartPoint()
      transform = AffineTransform()
      translate = AffineTransform()
      scale = AffineTransform()
      #Aus irgendeinem grund war halbe bildgroesse genau richtig
      #boeses voodoo!
      #die Renderinfo enthaelt keinen hinweise ueber die dimension des bildes in der seite
      #darum war ich gezwungen zu testen was am besten geht
      scale.setToScale(0.5,0.5)
      translate.setToTranslation(start.get(0), d.getHeight() - start.get(1)-bi.getHeight()/2) 
      transform.concatenate(translate)  
      transform.concatenate(scale)
      
      self.g.drawRenderedImage(bi,transform)

   def linearInterpol3Color(self,c1,c2,c3,position):
      """
      liefert einen Farbwert fuer die position zurueck
      position in einen [0,1] normierten Gradienten von c1 nach c3 ueber c2
      wobei c1-3 Instanzen von java.awt.Color sein sollten 
      oder sich aehnlich verhalten sollten
      """
      if position == 0.5 :
         return c2
      elif position <= 0:
         return c1
      elif position >= 1:
         return c3
      elif position < 0.5:
         return self.linearInterpolColor(c1,c2,position*2)
      else:
         return self.linearInterpolColor(c2,c3,(position-0.5)*2)

   def linearInterpolColor(self,c1,c2,position):
      """
      liefert einen Farbwert fuer die position zurueck
      position in einen [0,1] normierten Gradienten von c1 nach c2
      wobei c1-2 Instanzen von java.awt.Color sein sollten 
      oder sich aehnlich verhalten sollten
      """
      r = self.linearInterpol(c1.getRed(),c2.getRed(),position)
      g = self.linearInterpol(c1.getGreen(),c2.getGreen(),position)
      b = self.linearInterpol(c1.getBlue(),c2.getBlue(),position)
      #print r,g,b
      ret = Color(int(r),int(g),int(b))
      return ret

   def linearInterpol(self,startval, stopval, position):
      """
      liefert den [0,1] normierten Wert eines intervalls startval:stopval
      
      z.B.: self.linearInterpol(300,400,0.5) = 350
            self.linearInterpol(300,400, 1 ) = 400
            self.linearInterpol(300,400, 0 ) = 300
      """
      ret = (startval*(1-position))+(stopval*(position))
      return ret

