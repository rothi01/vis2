from com.itextpdf.text.pdf.parser import RenderListener
from com.itextpdf.text.pdf.parser import TextExtractionStrategy
import math
import sys

class TextAnalyzer(RenderListener):
   """
   TextAnalyzer-Klasse
   Implementiert RenderListener aus iText
   Rendert nicht sondern macht Satzzusammenfuehrung und Block detection
   """
   def __init__ (self): 
      self.trenner = '/'
      self.akt_satz = -1
      self.akt_block = -1
      self.resultingText = ''
      self.blockMapping = dict()
      self.satzMapping = dict()
      self.satz_blockMapping = []
      self.block_satzMapping = []
      self.saetze = []
      self.bloecke = []
      self.wordMapping = {}
      
      self.prev_x = -1
      self.prev_y = -1
      self.prev_ex = -1
      self.prev_ey = -1
      self.prev_text = ''
      self.prev_ly = -1
      self.prev_line_height = -1
      self.letter_spaceing_threshold = 1

      #Flag fuer worttren-
      #nung
      self.wort_trennung = 0
      

   def beginTextBlock(self):
      """
      Callback fuer den Parser
      notifiy wenn Text beginnt
      
      startet einen neuen Block und einen neuen satz
      """
      self.blockIncrement()
      self.satzIncrement()
      
      
   def renderText(self, renderInfo):
      """
      Callback fuer den Parser

      here comes the magic!
      
      Primitive Textanalyse, einteilung nach bloecken, saetzen und absaetzen
      (keine solchen
            Absaetze - diese Art Absatz wird nicht erkannt.
      
      Nur leere Zeilen werden als Absatz erkannt
      Mehrspaltiges Layout wird erkannt
      unterschiedliche textgroessen
      in text fussnoten
      """
      text = renderInfo.getText()
      
      #aktueller Font
      font = (renderInfo.getFont().getFullFontName())
      font = font[0][3]

      #aktuelle baseline x/y end
      ba_start = renderInfo.getBaseline().getStartPoint()      
      akt_x = ba_start.get(0)
      akt_y = ba_start.get(1)

      #aktuelle baseline x/y start
      ba_end = renderInfo.getBaseline().getEndPoint()
      akt_ex = ba_end.get(0)
      akt_ey = ba_end.get(1)

      # aktuelles unteres Zeilenende
      des_start = renderInfo.getDescentLine().getStartPoint()
      akt_ly = des_start.get(1)

      #aktuelles oberes zeilenende
      as_start = renderInfo.getAscentLine().getStartPoint()
      akt_hy = as_start.get(1)

      akt_line_height = akt_hy - akt_ly
      #init letzten anfang mit aktuellem anfang
      if(self.prev_x == -1):
         self.prev_x = akt_x
         self.prev_y = akt_y
      #init letztes ende mit aktuellem anfang
      if(self.prev_ex == -1):
         self.prev_ex = akt_x
         self.prev_ey = akt_y
      #init letztes unteres ende mit aktuellem oberen
      if(self.prev_ly == -1):
         self.prev_ly = akt_hy
      if(self.prev_line_height == -1):
         self.prev_line_height = akt_line_height

      #Zitate sind eigene Saetze
      #if(text.startswith('"') or text.startswith("'")):
      #   self.satzIncrement()
      
      #Wenn wir noch in der selben Zeile sind wie gerade Eben
      if(self.prev_ey == akt_y):
         #print "akt:"+str(akt_x)+" - prev:"+str(self.prev_ex)+" = "+str(akt_x - self.prev_ex) + "fuer text:"+text 
         #wenn der abstand zum vorhergehenden text kleiner ist als der threshold dann gehoert dieser text zum vorhergehenden wort
         if((akt_x - self.prev_ex) < self.letter_spaceing_threshold):
            self.addText(text,self.prev_x, self.prev_y, '')
            self.addIndex(text,akt_x, akt_y)
         #Wenn der abstand groesser ist, ist es ein neues wort
         else:
            self.addText(text,akt_x,akt_y,' ')
            #da dies ein neues wort ist, muss die prev_x/y verschoben werden
            self.prev_x = akt_x
            self.prev_y = akt_y
         #in jedem fall aber das aktuelle wort speichern
         self.prev_text = text
         self.prev_ly = akt_ly
         self.prev_line_height = akt_line_height


      #wenn wir doch in der naechsten Zeile sind... gehen wir davon aus dass es ein neues wort ist (Ap- plication trennungen werden mal ignoriert)
      # neue Zeile kann einen Absatz bedeuten!
      else:
         space = ' '
         #Wenn in einer neuen Zeile das Flag worttrennung gesetzt ist, ist das erste wort, teil des vorhergehenden wortes
         if(self.wort_trennung):
            self.wort_trennung = 0
            self.cutTrenner(self.prev_x,self.prev_y,'-')
            space=''
            #self.addText(text,self.prev_x, self.prev_y, '')
            self.prev_text = self.getWordByCoords(self.prev_x, self.prev_y)
   
         line_spacing_threshold = (renderInfo.getAscentLine().getStartPoint().get(1) - renderInfo.getDescentLine().getStartPoint().get(1))*0.4
         #wenn abstand vorhergehende zeile unten zu dieser zeile oben > 1.5 * Zeilenhoehe -> Absatz
         #Absaetze beenden auch saetze
         if(((self.prev_ly - akt_hy) > line_spacing_threshold)):
            self.blockIncrement()
            self.satzIncrement()
            self.prev_text = text
         #bei mehrspaltigen Layouts wenn neue spalte kommt ist die differenz gross und negativ
         elif(abs(self.prev_ly - akt_hy)>40):
            self.blockIncrement()
            self.block_satzMapping[self.akt_block].append(self.akt_satz)
            self.prev_text = text
         #Wenn keine neue spalte und kein absatz ABER! eine andere zeilenhoehe dann wechsel zwischen text und header -> Absatz
         #Zahlen werden hiervon ausgenommen!
         elif((self.prev_line_height != akt_line_height) and not(text.isnumeric()) and not(self.prev_text.isnumeric())):
            self.blockIncrement()           
            self.satzIncrement()
            self.prev_text = text
         #wenn also zeilenhoehe ungleich ist aber irgendwelche ziffern dabei sind z.B. fussnoten
         elif(self.prev_line_height != akt_line_height):
            self.prev_text = text
         #Absatz oder auch nicht, neue Zeile immer ohne fuehrendes ' '
         self.addText(text,akt_x,akt_y,space)
         self.prev_ly = akt_ly
         self.prev_x = akt_x
         self.prev_y = akt_y
         self.prev_line_height = akt_line_height
         

      #in jedemfall verschiebt sich das neue vorhergehende wortende (Neuer Absatz oder nicht)
      self.prev_ex = akt_ex
      self.prev_ey = akt_ey
      
      #Wenn das aktuelle Wort mit einem '-' endet koennte eine worttrennung folgen
      if(text.endswith('-')):      
         self.wort_trennung = 1
      #Wenn das aktuelle Wort mit Satzzeichen endet gehen wir von Satzende aus. ("Heute ist der 1. Jannuar", "Herr M. Mayer" werden ignoriert) 
      elif(text.endswith('.') | text.endswith('?') | text.endswith('!') | text.endswith(':')):
         self.satzIncrement()
   

   def getBlockId(self, pos_x, pos_y):
      """
      liefert die id des blocks an position (pos_x, pos_y)
      geht natuerlich erst nach dem parsen
      """
      key = str(pos_x)+self.trenner+str(pos_y)
      blockid = self.blockMapping[key]
      return blockid
   
   def getSatzId(self, pos_x, pos_y):
      """
      liefert die id des Satzes an position (pos_x, pos_y)
      geht natuerlich erst nach dem parsen
      """
      key = str(pos_x)+self.trenner+str(pos_y)
      satzid = self.satzMapping[key]
      return satzid
   
   def getBlockCount(self):
      """
      Liefert die anzahl der bloecke
      """
      return len(self.block_satzMapping)

   def getSatzCount(self,blockid=-1):
      """
      liefert die anzahl der saetze fuer block mit blockid
      Wenn blockid -1 (default):
          dann anzahl der saetze dieser seite
      """
      if(blockid==-1):
         return len(self.satz_blockMapping)
      else:
         if(blockid < 0 or blockid >= self.getBlockCount()):
            return -1
         return len(self.block_satzMapping[blockid])

   def getBlockById(self, blockid):
      """
      Liefert den Text des Blocks mit index blockid
      """
      if(blockid < 0 or blockid >= self.getBlockCount()):
         return None
      else:
         ret = []
         for satzid in self.block_satzMapping[blockid]:
            ret.append(self.saetze[satzid])
         return ret

   def getBlockByCoords(self,pos_x, pos_y):
      """
      liefert den Text des Blocks an Position (pos_x,pos_y)
      """
      key = str(pos_x)+self.trenner+str(pos_y)
      blockid = self.blockMapping[key]
      return self.getBlockById(blockid)

   def getSatzIdsByCoords(self, pos_x, pos_y):
      """
      liefert eine liste an Satzids fuer den Block an der Position (pos_x,pos_y)
      """
      key = str(pos_x)+self.trenner+str(pos_y)
      return self.getSatzIdsByBlockId(self.blockMapping[key])

   def getSatzIdsByBlockId(self, blockid):
      """
      liefert eine liste an Satzids fuer den Block mit dem index blockid
      """
      if(blockid < 0 or blockid >= self.getBlockCount()):
         return None
      else:
         ret = []
         for satzid in self.block_satzMapping[blockid]:
            ret.append(satzid)
         return ret
      
   def getSatzById(self, satzid):
      """
      liefert den Text des satzes mit index satzid
      """
      return self.saetze[satzid]

   def getSatzByCoords(self,pos_x, pos_y):
      """
      liefert den Text des Satzes an Position (pos_x,pos_y)
      """
      return self.saetze[self.satzMapping[str(pos_x)+self.trenner+str(pos_y)]]

   def getWordByCoords(self, pos_x, pos_y):
      """
      Liefert den Text des Wortes an position (pos_x,pos_y)
      """
      key = str(pos_x)+self.trenner+str(pos_y)
      return self.wordMapping[key]

   

   def cutTrenner(self, pos_x, pos_y, char):
      """
      Fuehrt mit - getrennte worte zusammen
      Bug: "-" wird nicht entfernt
      """
      key = str(pos_x)+self.trenner+str(pos_y)
      tmp = self.wordMapping[key]
      self.wordMapping[key] = tmp[:-2]
      #print tmp[:-2] + " -> "+self.wordMapping[key]

   def addIndex(self,text,pos_x,pos_y):
      """
      fuege die aktuelle position (pos_x,pos_y) den Indexlisten hinzu

      verbinde faelschlich getrennte worte
      Parser liest manche worte in teilen aus:
      z.B.  diff icult -> difficult
            A B S T R A C T -> ABSTRACT
      """
      key = str(pos_x)+self.trenner+str(pos_y)
      self.satzMapping[key] = self.akt_satz
      self.blockMapping[key] = self.akt_block
      if(key in self.wordMapping):
         self.wordMapping[key] += text 
      else:
         self.wordMapping[key] = text
      return key
   
   def addText(self, text, pos_x, pos_y, space=" "):
      """
      verarbeite diesen Text
      1. index anlegen
      2. zu satz und block hinzufuegen
      """
      key = self.addIndex(text,pos_x,pos_y)
      self.saetze[self.akt_satz] += space + text
      self.bloecke[self.akt_block] += space + text

   def satzIncrement(self):
      """
      neuersatz!
      legt einen neuen satz an, macht ihn zum aktuellen Satz
      und fuegt dem dem aktuellen block hinzu
      """
      self.akt_satz +=  1
      self.saetze.insert(self.akt_satz,'')
      self.satz_blockMapping.insert(self.akt_satz,self.akt_block)
      self.block_satzMapping[self.akt_block].append(self.akt_satz)
   def blockIncrement(self):
      """
      legt einen neuen block an
      und macht ihn zum aktuellen block
      """
      self.akt_block += 1
      self.bloecke.insert(self.akt_block,'')
      self.block_satzMapping.insert(self.akt_block,[])


      

