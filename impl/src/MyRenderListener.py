from com.itextpdf.text.pdf.parser import RenderListener
class MyRenderListener(RenderListener):
   def __init__ (self):
      
      print "This is MyRenderListener class which implements the java interface com.itextpdf.text.pdf.parser.RenderListener"
   def beginTextBlock(self):
      print 'begin'      
   def endTextBlock(self):
      print 'end'
   def renderText(self, renderInfo):
      #print renderInfo.getText(),
      #print dir(renderInfo)
      #print dir(renderInfo.getAscentLine())
      bb = renderInfo.getAscentLine().getBoundingRectange()
      print "x:"+str(bb.x) +" y:"+ str(bb.y)+" w:"+str(bb.width)+" h:"+str(bb.height),
   def renderImage(self, renderInfo):
      print 'image'
