from javax.swing import JFrame
from javax.swing import JComponent
from java.awt import Toolkit
from java.awt.event import ActionListener
from java.awt.event import MouseListener
from java.awt.event import ComponentListener
from javax.swing import JMenuBar
from javax.swing import JMenu
from javax.swing import JMenuItem
from javax.swing import JPanel
from javax.swing import JTextField
from javax.swing import JScrollPane
from javax.swing import JTextArea
from javax.swing.table import TableCellRenderer
from java.awt import BorderLayout
from java.awt import GridLayout
from java.awt import Dimension
from java.awt import Color
from javax.swing import JTable

from src import utilities
from src import MyCanvas
from src import TextAnalyzer
from src import RenderPage

import math

class Window(JFrame):
   """
   Basisfenster-Klasse Extends javax.swing.JPanel
   
   Eigentlich die ganze GUI-Verwaltung 
   
   Layout:
      ScrollPanel links mit seitenuebersicht
      hautppanel mittig
   """
   def __init__(self, reader):
      """
      Konstruktor
      init JFrame
      setHeader und menu
      init pages
      """
      JFrame.__init__(self)
      self.setBounds(50,50,1000,500)
      self.setTitle("Readability Visualisierung 2")
      
      self.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      #CG-Icon :-)
      self.setIconImage(Toolkit.getDefaultToolkit().getImage("res/icon.png"))
      self.reader = reader
      self.pages = []
      self.tables = []
      
      #Menu Bar
      bar = JMenuBar()
      self.setJMenuBar(bar)
      filemenu = JMenu("File")
      open_pdf = JMenuItem("open")
      open_pdf.addActionListener(OpenPDF(self.reader))      
      filemenu.add(open_pdf)
      quit_program = JMenuItem("quit")
      quit_program.addActionListener(QuitProgram(self))
      filemenu.add(quit_program)
      bar.add(filemenu)

      analysisMenu = JMenu("Analysis")
      changeMainView = JMenuItem("toggle MainView")
      changeMainView.addActionListener(ChangeMain(self))
      analysisMenu.add(changeMainView)
      bar.add(analysisMenu)
      

      #init der pages, tables, etc
      self.add(self.pageViewInit(500))
      self.tableViewInit(500)
      self.add(self.pageOverviewInit(300),BorderLayout.WEST)

      self.setVisible(1)
      ##TODO reimplement resizer
      #self.addComponentListener(Resizer())

   def tableViewInit(self,width):
      """
      initialisiere anzeige fuer TabellenView
      mit Scroll und allem was dazugehoert
      """
      self.center_2 = JPanel(BorderLayout())
      scroll = JScrollPane(self.center_2)
      scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
      scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
      #scroll.setPreferredSize(Dimension(width,int(math.sqrt(2)*width)))
      scroll.getVerticalScrollBar().setUnitIncrement(50);
      self.scroll_center_2 = scroll
      return scroll
      
   def pageViewInit(self,width):
      """
      initialisiere anzeige fuer PageView
      mit Scroll und allem was dazugehoert
      """
      self.center = JPanel()
      scroll = JScrollPane(self.center)
      scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
      scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
      scroll.setPreferredSize(Dimension(width,int(math.sqrt(2)*width)))
      scroll.getVerticalScrollBar().setUnitIncrement(50);
      self.scroll_center = scroll
      return scroll

   def pageOverviewInit(self, width):
      """
      initialisiere anzeige fuer Seitenueberblick links
      mit Scroll und allem was dazugehoert
      """
      overview = JPanel(GridLayout(1,1))
      self.overview = overview

      scroll = JScrollPane(overview)
      scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
      scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
      scroll.setPreferredSize(Dimension(width,200))
      scroll.getVerticalScrollBar().setUnitIncrement(50);
      return scroll
   
   def resetPageOverview(self,pages=None):
      """
      leere seitenueberblick
      und lege neue seiten 
      """
      if(pages == None):
         if(self.pages == None):
            pages = 0
         else:
            pages = len(self.pages)
      self.tables = []
      self.overview.removeAll()
      self.overview.setLayout(GridLayout(pages,1,5,5))
      self.pages = []
      for i in range(pages):
         page = RenderPage.RenderPage(self, i+1,self.overview.getWidth())
         self.pages.append(page)
         self.overview.add(page)
      self.setVisible(1)

   def setSelectedPage(self,parser,pagenr=1):
      """
      callback fuer maouslistener
      diese Seite ist angeklickt worden und damit markiert
      setze jeweils die pagenr (beginnend index 1 - Bloede idee das von itext zu uebernehmen!! macht nur probleme) 
      als aktuelle page
      aktualisiere tabelView/pageview
      """
      if(pagenr > len(self.pages)):
         pagenr = len(self.pages)
      if(pagenr < 1):
         pagenr = 1
      
      #aktualisiere pageView
      centerPage = RenderPage.RenderPage(self,pagenr,self.center.getWidth())
      centerPage.content.parser = parser
      self.center.removeAll()
      self.center.add(centerPage)
      
      #Aktualisiere tableview
      self.center_2.removeAll()
      if(len(self.tables) < pagenr):
         for i in range(pagenr-len(self.tables)-1,pagenr):
            self.initTable(i)
      self.center_2.add(self.tables[pagenr-1].getTableHeader(), BorderLayout.NORTH);
      self.center_2.add(self.tables[pagenr-1],BorderLayout.CENTER)
      self.setVisible(1)
      
   def initTable(self,pagenr):
      """
      initialisiere jTable fuer dies seitennummer(index origin 0)
      """
      #init tableheader
      feat = self.reader.features
      names = ['Satz']
      for f in feat:
         names.append(f.name)
      
      #init helper und dataarray
      analyzer = self.pages[pagenr].analyzer
      data = []
      gradient = self.pages[pagenr].content.linearInterpol3Color

      #befuelle dataarray mit allen saetzen und ratings
      for i in range(analyzer.getSatzCount()):
         satz = analyzer.getSatzById(i)
         ratings = self.reader.getSatzRatingsById(pagenr,i)
         rat = self.reader.features[0].mean(ratings)
         color = gradient(Color.BLUE,Color.WHITE,Color.RED,rat)
         line = [SatzWrapper(satz,color)]
         for r in ratings:
            line.append(gradient(Color.BLUE,Color.WHITE,Color.RED,r))
         data.append(line)
      
      #init der jtable   
      table = JTable(data,names)
      table.setRowHeight(55)
      
      #init cellrenderer fuer features
      renderer = ColorRenderer()
      for i in range(0,table.getColumnCount()):
         col=table.getColumnModel().getColumn(i)
         col.setCellRenderer(renderer)
         col.setPreferredWidth(65)

      #init cellrenderer fuer saetze (textarea mit umbruch!)
      col = table.getColumnModel().getColumn(0);
      col.setPreferredWidth(600)
      col.setCellRenderer(SatzRenderer());
      table.setVisible(1)
      self.tables.insert(pagenr,table)


class OpenPDF(ActionListener):
   """
   Listener fuer file->open
   """
   def __init__(self, reader):
      self.reader = reader
   def actionPerformed(self, e):
      input_ = utilities.FileChooser('..')
      if(input_ != None):
         pfad = input_.getAbsolutePath()
         self.reader.parseFile(pfad)

class QuitProgram(ActionListener):
   """
   listener fuer file->quit
   """
   def __init__(self, window):
      self.window = window
   def actionPerformed(self, e):
      self.window.dispose()

class ChangeMain(ActionListener):
   """
   listener fuer analysis->toggleMainview
   """
   def __init__(self, window):
      self.window = window
   def actionPerformed(self, e):
      self.window.remove(self.window.scroll_center)
      self.window.scroll_center, self.window.scroll_center_2 = self.window.scroll_center_2, self.window.scroll_center
      self.window.add(self.window.scroll_center)

      #pfui-deifl hack: fuer resize im anderen viewmode
      dim = self.window.getSize()
      self.window.setSize(Dimension(1,1))
      self.window.setSize(dim)
      self.window.repaint()
      #ende pfui-deifl hack

class Resizer(ComponentListener):
   """
   ungenutzer resizer
   """
   def componentResized(self, e):
      e.getSource().scroll.setPreferredSize(Dimension(int(e.getSource().getWidth()*0.2),e.getSource().getHeight()))
      e.getSource().resetPageOverview();


class ColorRenderer(JTextArea, TableCellRenderer):
   """
   Renderer fuer features
   Zeigt Ratings als farbfeld an
   """
   def getTableCellRendererComponent(self,table, color,isSelected,hasFocus, row, column):
      self.setBackground(color)
      return self

class SatzRenderer(JTextArea, TableCellRenderer):
   """
   Renderer fuer saetze
   macht Zeilenumbruch und Farbhinterlegung
   """
   def __init__(self):
      self.setLineWrap(1)
   def getTableCellRendererComponent(self,table, data,isSelected,hasFocus, row, column):
      col = table.getColumnModel().getColumn(column)
      self.setBackground(data.color)
      self.setText(data.text)
      return self

class SatzWrapper:
   """
   wrapper fuer satz+rating
   damit beides auf einmal in der jtable drin ist
   """
   def __init__(self,text,color):
      self.text = text
      self.color = color

