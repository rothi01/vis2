from javax.swing import JPanel
from java.awt import Color
from java.awt import Dimension
from java.awt.event import MouseListener
from src import MyCanvas
from src import TextAnalyzer
import math
class RenderPage(JPanel):
   """
   Klasse fuer einzelseiten
   Seitennummer mit 1 beginnend
   """
   def __init__(self,window,pagenr,size):
      JPanel.__init__(self)
      self.analyzer = TextAnalyzer.TextAnalyzer()
      self.pagenr = pagenr
      self.selected_color = Color.BLUE
      self.default_color = Color.WHITE
      self.addMouseListener(PageClick(self,window))
      self.setBackground(self.default_color)
      self.setWidth(size)
      self.content = MyCanvas.MyCanvas(self.pagenr,int(size*0.95),window.reader)
      self.add(self.content)

   def setWidth(self,a):
      """
      setzt die aktuelle seite auf eine breite, aber immer im hoehenverhaeltnis von A4
      """
      self.setPreferredSize(Dimension(a,int(math.sqrt(2)*a)))



   def select(self):
      """
      callback fuer seitenueberblick in der GUI links
      wenn diese seite Angeklickt wird dann wird sie selektiert und blau umrandet
      """
      self.selected = 1
      self.setBackground(self.selected_color)

   def unselect(self):
      """
      callback fuer seitenueberblick in der GUI links
      unselect wenn eine andere Seite selected wird
      """
      self.selected = 0
      self.setBackground(self.default_color)

   def setParser(self, parser):
      """
      callback fuer PDF-Reader
      Wenn Gui den Reader zum parsen triggert, setzt der Reader bei der GUI neue Pages
      und setzt bei diesen den Parser
      dazu wird dies funktion aufgerufen
      """
      self.content.parser = parser
      self.content.parser.processContent(self.pagenr,self.analyzer)
      #print self.analyzer.saetze

class PageClick(MouseListener):
   """
   MouseListener
   """
   def __init__(self,page,window):
      """
      konstruktor
      RenderPage instanz und GUI Instanz
      fuer spaeter speichern
      """
      self.page = page
      self.window = window

   def mouseClicked(self, e):
      """
      Mouse_Klick
      Wenn diese Seite angeklickt wird dann unselect alle seiten
      und select dieser seite 
      """
      for p in self.window.pages:
         p.unselect()
      self.page.select()
      self.window.setSelectedPage(self.page.content.parser,self.page.pagenr)


